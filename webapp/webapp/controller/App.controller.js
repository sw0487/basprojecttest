sap.ui.define(
    [
        "sap/ui/core/mvc/Controller"
    ],
    function(BaseController) {
      "use strict";
  
      return BaseController.extend("com.istn.sol.education.webapp.controller.App", {
        onInit() {
        }
      });
    }
  );
  