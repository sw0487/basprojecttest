/* global QUnit */

sap.ui.require(["com/istn/sol/education/webapp/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
